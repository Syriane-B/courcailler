<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SecurityControllerTest extends WebTestCase
{
    public function testLoginRoute()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');

        $this->assertResponseIsSuccessful();
        $button = $crawler->selectButton('submit');
        $form = $button->form();

        //Try with unexisting email adress
        $crawler = $client->submit($form, [
           'email' => 'azerty@azerty.com',
           'password' => 'password'
        ]);
        $client->followRedirect();
        $this->assertSelectorTextContains('.alert', 'Votre email ou votre mot de passe n\'est pas correct');

        //Try with wrong password
        $crawler = $client->submit($form, [
            'email' => 'f@d.fr',
            'password' => 'password'
        ]);
        $client->followRedirect();
        $this->assertSelectorTextContains('.alert', 'Votre email ou votre mot de passe n\'est pas correct');

        //Try with valid email and password
        $crawler = $client->submit($form, [
            'email' => 'f@d.fr',
            'password' => 'fififou'
        ]);
        $client->followRedirect();
        $this->assertSelectorTextContains('h5', 'Courcaillez !');
    }
}
