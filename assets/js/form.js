document.addEventListener('DOMContentLoaded', function() {
    //initialisation of materialize css date picker
    let elems = document.querySelectorAll('.datepicker');
    let dapickers = M.Datepicker.init(elems, {
        defaultDate: new Date(1993,6,29),
        setDefaultDate: true,
        selectMonths: true,
        yearRange: 100,
        maxDate: new Date(2010,1,1),
        format: "dd/mm/yyyy"
    });
});