/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */
// const $ = require('jquery');

// any CSS you require will output into a single css file (app.scss in this case)
require('../css/app.scss');
require('materialize-css/dist/js/materialize.min');
import './form';
import './main';
// //Initialization on materialize css drop down
// let dropDownElems = document.querySelectorAll('.collapsible');
// let instances = M.Collapsible.init(dropDownElems);