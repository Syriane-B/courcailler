//on DOM ready
const $ = require('jquery');
window.addEventListener("DOMContentLoaded", (event) => {
    //Courcaillet characters count at creation
    let $ccContentInput = document.getElementById('new_courcaillet_content');
    $ccContentInput.addEventListener('keyup', function (e){
        document.getElementById('ccContentCount').innerText = $ccContentInput.value.length;
    });

    //Modal window (delete my profile)
        let modalElems = document.querySelectorAll('.modal');
        let instances = M.Modal.init(modalElems);

    //Request on user in mention input
    //initialize axios request
    const axios = require('axios');
    //create allUser array
    let allUsers = [];

    //function to add autocomplete chips div content to the form input (to handle values in controller with the form datas)
    function chips2Input(){
        //get dom elements input field and chips div
        let instance = M.Chips.getInstance(document.getElementById('mentionsAutocomplete')),
            inpt = document.getElementById('new_courcaillet_mention');
        //emptying the input
        inpt.value =  null;
        //get all chips in the div chips and add it to the input (hidden)
        for(var i=0; i<instance.chipsData.length; i++){
            if(inpt.value == null)
                inpt.value = instance.chipsData[i].tag;
            else{
                inpt.value += instance.chipsData[i].tag + ' ';
            }
        }
    }
    //make axios request
    axios.get(`/all-users`)
        .then(function (response) {
            for (const user of response.data) {
                //for each user, push in the allUsers array the user pseudo as key and user avatar path as value (materialize chips format)
                allUsers[user.pseudo] = `/uploads/images/avatars/${user.avatarName}`;
            }
            //initialize chips from materialize css
            let elems = document.querySelectorAll('.chips-autocomplete');
            let instances = M.Chips.init(elems,
                {
                    autocompleteOptions: {
                        //give the all user json
                        data: allUsers
                    },
                    onChipAdd: chips2Input,
                    onChipDelete: chips2Input,
                    limit: 10
                });
        });
});
