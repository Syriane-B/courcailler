//wait for the DOM to be ready
document.addEventListener('DOMContentLoaded', function() {
    //Initialization on materialize css drop down
    let dropDownElems = document.querySelectorAll('.collapsible');
    let instances = M.Collapsible.init(dropDownElems);
    //Search button activation (show search popup)
    document.getElementById('searchBtn').addEventListener('click', function (e) {
        document.getElementById('searchPopUp').classList.toggle('hide')
    });
    //toggle the hide class on close button on search box pop up
    document.getElementById('searchBoxCloseBtn').addEventListener('click', function (e) {
        document.getElementById('searchPopUp').classList.toggle('hide')
    });

    //searchBar (in tpl-part/header.html.twig)
    //initialize axios request
    const axios = require('axios');
    //get DOM element for search bar
    let $searchInput = document.getElementById('search');
    let $usersResultBox = document.getElementById('usersResultBox');
    let $hashtagResultBox = document.getElementById('hashtagResultBox');
    //launch the request at every keyUp on searchBar input
    $searchInput.addEventListener('keyup', function (e) {
        if ($searchInput.value !== '')  {
            //get the searchBar value (to launch both requests on users and hashtags
            let searchText = $searchInput.value;
            //make axios request on users with input text in parameter
            axios.get(`/users/${searchText}`)
                .then(function (response) {
                    if (response.data.length > 0) {
                        $usersResultBox.innerHTML = '';
                        //for each user add chips in the search box
                        for (const user of response.data) {
                            let $newChip = document.createElement('div');
                            $newChip.classList.add('chip');
                            $newChip.innerHTML =
                            `
                                <img src="/uploads/images/avatars/${user.avatarName}" alt="${user.pseudo} avatar">
                                <a href="/profile/${user.id}">
                                    @${user.pseudo}
                                </a>
                             `;
                            $usersResultBox.appendChild($newChip);
                        }
                    }
                    //if on one match, inform the user
                    else {
                        $usersResultBox.innerHTML = '';
                        let $noResult = document.createElement('p');
                        $noResult.innerText = 'Aucune @caille ne correspond à votre recherche';
                        $usersResultBox.appendChild($noResult);
                    }
                });

            //make axios request on hashtag with input text in parameter
            axios.get(`/axioshashtag/${searchText}`)
                .then(function (response) {
                    if (response.data.length > 0) {
                        $hashtagResultBox.innerHTML = '';
                        // for each user add chips in the search box
                        for (const hashtag of response.data) {
                            let $newChip = document.createElement('div');
                            $newChip.classList.add('chip');
                            $newChip.innerHTML =
                                `
                                <a href="/hashtag/${hashtag.name}">
                                    #${hashtag.name}
                                </a>
                             `;
                            $hashtagResultBox.appendChild($newChip);
                        }
                    }
                    //if on one match, inform the user
                    else {
                        $hashtagResultBox.innerHTML = '';
                        let $noResult = document.createElement('p');
                        $noResult.innerText = 'Aucun #Hashtag ne correspond à votre recherche';
                        $hashtagResultBox.appendChild($noResult);
                    }
                });
        }
    })
});