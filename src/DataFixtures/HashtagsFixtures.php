<?php

namespace App\DataFixtures;

use App\Entity\Hashtag;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class HashtagsFixtures extends Fixture
{
    public const HASHTAG_REFERENCE = 'hashtag_';

    public function load(ObjectManager $manager)
    {

//        Fake users
        $faker = Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 40; $i++) {
            $hastag = new Hashtag();
            $hastag->setName($faker->word.$i);

            $manager->persist($hastag);

            $this->addReference(self::HASHTAG_REFERENCE . $i, $hastag);

            $manager->flush();
        }
    }
}
