<?php

namespace App\DataFixtures;

use App\Entity\Courcaillet;
use App\DataFixtures\UsersFixtures;
use App\DataFixtures\HashtagsFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class CourcailletsFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        //Fake users
        $faker = Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 50; $i++) {
            $courcaillet = new Courcaillet();
            $courcaillet->setContent($faker->text(140));
            $courcaillet->setDate($faker->dateTime('now', null));

            $courcaillet->setUserNbr($this->getReference(UsersFixtures::USER_REFERENCE.$faker->numberBetween(0, 29)));
            $nbMentions = $faker->numberBetween($min = 1, $max = 10);
            //Add random number of mention
            for ($j = 0; $j < $nbMentions; $j++) {
                $courcaillet->addMention($this->getReference(UsersFixtures::USER_REFERENCE.$faker->numberBetween(0, 29)));
            }
            //Add random number of hashtags
            $nbHtag = $faker->numberBetween($min = 1, $max = 12);
            for ($k = 0; $k < $nbHtag; $k++) {
                $courcaillet->addHashtag($this->getReference(HashtagsFixtures::HASHTAG_REFERENCE.$faker->numberBetween(0, 39)));            }
            $manager->persist($courcaillet);
            $manager->flush();
        }
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return array(
        UsersFixtures::class,
        HashtagsFixtures::class,
        );
    }
}