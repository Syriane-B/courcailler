<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UsersFixtures extends Fixture
{
    public const USER_REFERENCE = 'user_';
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder){
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
//        Fake users
        $faker = Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 30; $i++) {
            $user = new User();
            $user->setName($faker->name);
            $user->setFirstname($faker->firstName);
            $user->setEmail($faker->email);
            $user->setBirthday($faker->dateTime('now', null));
            $user->setRegisterDate($faker->dateTime('now', null));
            $user->setAvatarUpdatedAt(new \DateTime('@'.strtotime('now')));
            $user->setPseudo($faker->firstName . $faker->word);
            $user->setPassword(
                $this->passwordEncoder->encodePassword(
                    $user,
                    'password'
                )
            );
            $user->setAvatarName($faker->image('/home/syriane/code/courcailler/public/uploads/images/avatars/', 200,200, null, false));
            $manager->persist($user);

            $this->addReference(self::USER_REFERENCE . $i, $user);

            $manager->flush();
        }
    }
}
