<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191009125845 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE hashtag (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, UNIQUE INDEX UNIQ_5AB52A615E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE hashtag_courcaillet (hashtag_id INT NOT NULL, courcaillet_id INT NOT NULL, INDEX IDX_42CD0F83FB34EF56 (hashtag_id), INDEX IDX_42CD0F83B3E06AE8 (courcaillet_id), PRIMARY KEY(hashtag_id, courcaillet_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE courcaillet (id INT AUTO_INCREMENT NOT NULL, user_nbr_id INT NOT NULL, content VARCHAR(150) NOT NULL, date DATETIME NOT NULL, INDEX IDX_4417C1DC5DAA84AC (user_nbr_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE courcaillet_user (courcaillet_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_DF3E829DB3E06AE8 (courcaillet_id), INDEX IDX_DF3E829DA76ED395 (user_id), PRIMARY KEY(courcaillet_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, firstname VARCHAR(255) NOT NULL, birthday DATE NOT NULL, pseudo VARCHAR(50) NOT NULL, email VARCHAR(100) NOT NULL, password VARCHAR(100) NOT NULL, register_date DATE NOT NULL, UNIQUE INDEX UNIQ_8D93D64986CC499D (pseudo), UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE hashtag_courcaillet ADD CONSTRAINT FK_42CD0F83FB34EF56 FOREIGN KEY (hashtag_id) REFERENCES hashtag (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE hashtag_courcaillet ADD CONSTRAINT FK_42CD0F83B3E06AE8 FOREIGN KEY (courcaillet_id) REFERENCES courcaillet (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE courcaillet ADD CONSTRAINT FK_4417C1DC5DAA84AC FOREIGN KEY (user_nbr_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE courcaillet_user ADD CONSTRAINT FK_DF3E829DB3E06AE8 FOREIGN KEY (courcaillet_id) REFERENCES courcaillet (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE courcaillet_user ADD CONSTRAINT FK_DF3E829DA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE hashtag_courcaillet DROP FOREIGN KEY FK_42CD0F83FB34EF56');
        $this->addSql('ALTER TABLE hashtag_courcaillet DROP FOREIGN KEY FK_42CD0F83B3E06AE8');
        $this->addSql('ALTER TABLE courcaillet_user DROP FOREIGN KEY FK_DF3E829DB3E06AE8');
        $this->addSql('ALTER TABLE courcaillet DROP FOREIGN KEY FK_4417C1DC5DAA84AC');
        $this->addSql('ALTER TABLE courcaillet_user DROP FOREIGN KEY FK_DF3E829DA76ED395');
        $this->addSql('DROP TABLE hashtag');
        $this->addSql('DROP TABLE hashtag_courcaillet');
        $this->addSql('DROP TABLE courcaillet');
        $this->addSql('DROP TABLE courcaillet_user');
        $this->addSql('DROP TABLE user');
    }
}
