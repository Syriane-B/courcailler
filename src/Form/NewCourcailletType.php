<?php

namespace App\Form;

use App\Entity\Courcaillet;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewCourcailletType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('content',TextType::class, [
                'label' => 'Nouveau courcaillet',
                'attr' => ['maxlength' => 140, 'autocomplete' => 'off']
            ])
            ->add('mention', TextType::class, [
                'label' => 'Mentions',
                'required'   => false,
                'mapped'=>false,
                'attr'=>[
                    'autocomplete' => 'off',
                    'class' => 'hide'
                    ]
            ])
            ->add('hashtags',TextType::class, [
                'label' => '#',
                'required'   => false,
                'mapped'=>false,
                'attr'=>['autocomplete' => 'off']
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Courcaillet::class,
        ]);
    }
}

