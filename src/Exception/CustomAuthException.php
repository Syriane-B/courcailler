<?php


namespace App\Exception;


use Symfony\Component\Security\Core\Exception\AccountStatusException;

class CustomAuthException extends AccountStatusException
{
    /**
     * {@inheritdoc}
     */
    public function getMessageKey()
    {
        return 'Vous n\'avez pas activé votre compte, rendez - vous dans votre boîte mail et suivez le lien d\'activation !';
    }
}