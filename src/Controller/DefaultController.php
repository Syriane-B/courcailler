<?php

namespace App\Controller;

use App\Repository\CourcailletRepository;
use App\Repository\HashtagRepository;
use App\Repository\UserRepository;
use App\Service\FollowerService;
use Knp\Component\Pager\PaginatorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    private $params;
    private $entityManager;
    private $courcailletRepository;
    private $hashtagRepository;
    private $userRepository;
    private $followerService;

    public function __construct(
        EntityManagerInterface $entityManager,
        CourcailletRepository $courcailletRepository,
        HashtagRepository $hashtagRepository,
        UserRepository $userRepository,
        ParameterBagInterface $params,
        FollowerService $followerService
    )
    {
        $this->entityManager = $entityManager;
        $this->courcailletRepository = $courcailletRepository;
        $this->hashtagRepository = $hashtagRepository;
        $this->userRepository = $userRepository;
        $this->params = $params;
        $this->followerService = $followerService;
    }

    /**
     * @Route("/", name="start")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function start(Request $request, PaginatorInterface $paginator)
    {
        //get the connected user
        $connectedUser = $this->getUser();
        //if the user follow other cailles, get the most recents courcaillers from them
        if (count($connectedUser->getfollowing()) > 0) {
            $query = $this->followerService->getMostRecentPostFromFollowing($connectedUser);
            $pagination = $paginator->paginate(
                $query,
                $request->query->getInt('page', 1), /*page number*/
                10 /*limit per page*/
            );
            $courcailletTitle = 'Derniers courcaillets de mes @Cailles';
            //if the connected user don't follow any one, show the last courcaillet of the every one
        } else {
            $query = $this->courcailletRepository->getLastCourcaillets();
            $pagination = $paginator->paginate(
                $query,
                $request->query->getInt('page', 1), /*page number*/
                10 /*limit per page*/
            );
            $courcailletTitle = 'Derniers courcaillets publiés sur le site';
        }
//        dump($pagination);exit;
        return $this->render('default/start.html.twig', [
            'pagination' => $pagination,
            'courcailletTitle' => $courcailletTitle
        ]);
    }
    /**
     * @Route("/rgpd", name="rgpd")
     * @return Response
     */
    public function rgpd(Request $request, PaginatorInterface $paginator)
    {
        return $this->render('default/rgpd.html.twig');
    }
}
