<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\UserToken;
use App\Form\RegistrationFormType;
use App\Repository\UserRepository;
use App\Security\UserAuthAuthenticator;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Swift_Mailer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use App\Service\GenerateTokenService;

class RegistrationController extends AbstractController
{
    private $params;
    private $entityManager;
    private $mailer;
    private $generateToken;
    private $userRepository;

    public function __construct(
        ParameterBagInterface $params,
        EntityManagerInterface $entityManager,
        Swift_Mailer $mailer,
        GenerateTokenService $generateToken,
        UserRepository $userRepository
    )
    {
        $this->params = $params;
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
        $this->generateToken = $generateToken;
        $this->userRepository = $userRepository;
    }

    /**
     * @Route("/register", name="app_register")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param GuardAuthenticatorHandler $guardHandler
     * @param UserAuthAuthenticator $authenticator
     * @return Response
     * @throws Exception
     */
    public function register(
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder,
        GuardAuthenticatorHandler $guardHandler,
        UserAuthAuthenticator $authenticator
    ): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );
            //set the register date of the user to now
            $user->setRegisterDate(new DateTime('@'.strtotime('now')));

            //create a user token and set the registration token and link it to the user
            $userToken = new UserToken();
            $userToken->setRegistration($this->generateToken->generate());
            $user->setUserToken($userToken);

            $user->setActiveAccount(false);

            //set default avatar if the users didn't uploaded one (the file upload is handeled by Vich Uploader
            $avatarFile = $form['avatarFile']->getData();
            if (empty($avatarFile)) {
                $user->setAvatarName('default-avatar.jpeg');
                $user->setAvatarUpdatedAt(new DateTime('@'.strtotime('now')));
            }
            $this->entityManager->persist($user);
            $this->entityManager->flush();

            //Create a flash message to inform the user of its registration success and the email confirmation
            $this->addFlash('success', 'Votre inscription à bien été prise en compte ! Vous allez recevoir un email de confirmation. Avant de vous connecter, pensez à activer votre compte en suivant le lien de l\'email');

            //send confirmation email
            $message = new \Swift_Message();
            $message->setSubject("Vous vous êtes inscrit sur Courcaillez !");
            $message->setTo($user->getEmail());
            $message->setFrom('inscription@courcaillez.fr');
            $message->setBody(
                $this->renderView('emails/registration.html.twig', [
                        'userToken' => $userToken->getRegistration(),
                        'userId' => $user->getId()
                    ]), 'text/html'
            );
            $this->mailer->send($message);
            return $this->redirectToRoute('app_login');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView()
        ]);
    }

    /**
     * @Route(
     *      "/activate/{token}/{userId}",
     *      name="activate"
     * )
     * @param string $token
     * @param int $userId
     *
     * @return RedirectResponse
     */
    public function registrationActivation(int $userId, string $token)
    {
        $user = $this->userRepository->find($userId);
        //if the user account is already active, redirect to login route
        if ($user->getActiveAccount()){
            return $this->redirectToRoute('app_login');
        }
        //get the user registration token
        $userTokenRegistration = $user->getUserToken()->getRegistration();
        //check if the token given in url match the user registration token
        if ($token == $userTokenRegistration){
            //activate the account and redirect to login route
            $user->setActiveAccount(true);
            $this->entityManager->persist($user);
            $this->entityManager->flush();
            //Create a flash message to inform the user of its account activation
            $this->addFlash('success', 'Votre compte à bien été activé, vous pouvez maintenant vous connecter !');

            return $this->redirectToRoute('app_login');
        }
        else {
            //Create a flash message to inform the user of its account activation
            $this->addFlash('success', 'Une erreur s\'est produite dans l\'activation de votre compte, veuillez vérifier votre email d\'activation, ou re-créer votre compte');
            //return to the register route
            return $this->redirectToRoute('app_login');
        }
    }
}
