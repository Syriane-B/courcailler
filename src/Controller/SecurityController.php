<?php

namespace App\Controller;

use App\Entity\UserToken;
use App\Form\ForgotPasswordEmailType;
use App\Form\ResetPasswordType;
use App\Repository\UserRepository;
use App\Service\GenerateTokenService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use http\Exception;
use Swift_Mailer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    private $urlGenerator;
    private $userRepository;
    private $generateToken;
    private $entityManager;
    private $mailer;


    public function __construct(
        UrlGeneratorInterface $urlGenerator,
        UserRepository $userRepository,
        GenerateTokenService $generateToken,
        EntityManagerInterface $entityManager,
        Swift_Mailer $mailer
    )
    {
        $this->urlGenerator = $urlGenerator;
        $this->userRepository = $userRepository;
        $this->generateToken = $generateToken;
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
    }

    /**
     * @Route("/login", name="app_login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
//        dump($error);exit;

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        return new RedirectResponse(
            $this->urlGenerator->generate('app_login')
        );
    }

    /**
     * @Route("/forgot-password", name="forgot-password")
     *
     * @param Request $request
     * @return Response
     */
    public function forgotPassword(Request $request)
    {
        //create the forgot password email part form
        $form = $this->createForm(ForgotPasswordEmailType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //check if the email exist in the database
            $email = $form->get('email')->getData();
            $user = $this->userRepository->findOneBy(['email' => $email]);
            if($user !== null){
                //create and set forgot password token to the user
                $userToken = $user->getUserToken();
                //security created for user already in database when token table is added
                if ($userToken == null){
                    $userToken = new UserToken();
                    $user->setUserToken($userToken);
                    $this->entityManager->persist($user);
                }
                $userToken->setForgotPassword($this->generateToken->generate());
                $this->entityManager->persist($userToken);
                $this->entityManager->flush();

                //send a password reset email
                $message = new \Swift_Message();
                $message->setSubject("Courcaillez ! Réinitialisation de votre mot de passe");
                $message->setTo($user->getEmail());
                $message->setFrom('forgot-password@courcaillez.fr');
                $message->setBody(
                    $this->renderView('emails/forgot-password.html.twig', [
                        'userToken' => $userToken->getForgotPassword(),
                        'userId' => $user->getId()
                    ]), 'text/html'
                );
                $this->mailer->send($message);
            }
            //Create a flash message to inform the user of the email sending sucess
            $this->addFlash('success', "Un email a été envoyé à $email, suivez le lien pour choisir un nouveau mot de passe");
            return new RedirectResponse(
                $this->urlGenerator->generate('app_login')
            );
        }


        return $this->render('security/forgotPasswordEmail.html.twig', [
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route("/reset-password/{token}/{userId}",
     *     name="reset-password")
     * @param string $token
     * @param int $userId
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return RedirectResponse|Response
     */
    public function resetPassword(string $token, int $userId, Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = $this->userRepository->find($userId);
        //catch unknown user
        if($user !== null) {
            //check if the token match else redirect to login with flash message
            //get the user registration token
            $userTokenForgotPwd = $user->getUserToken()->getForgotPassword();
            //check if the token given in url match the user registration token
            if ($token == $userTokenForgotPwd) {
                //create the forgot password email part form
                $form = $this->createForm(ResetPasswordType::class);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    //set the new password
                    $user->setPassword(
                        $passwordEncoder->encodePassword(
                            $user,
                            $form->get('plainPassword')->getData()
                        )
                    );
                    $this->entityManager->persist($user);
                    //clean the forgot password token
                    $userToken = $user->getUserToken();
                    $userToken->setForgotPassword(NULL);
                    $this->entityManager->persist($userToken);
                    $this->entityManager->flush();
                    //Create a flash message to inform the user of reset password success
                    $this->addFlash('success', 'Votre mot de passe à été modifié avec succès ! Vous pouvez l\'utiliser dès maintenent');
                    return $this->redirectToRoute('app_login');
                }
                //the user match the token show the reset password form
                return $this->render('security/resetPassword.html.twig', [
                    'form' => $form->createView()
                ]);
            }
        }
        //Create a flash message to inform the user when the token doesn't match the user or the user doesn't exist
        $this->addFlash('success', "Une erreur s'est produite, veuillez faire une nouvelle demande de réinitialisation de mot de passe");
        return $this->redirectToRoute('app_login');
    }
}
