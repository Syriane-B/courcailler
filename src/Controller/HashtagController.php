<?php

namespace App\Controller;

use App\Repository\CourcailletRepository;
use App\Repository\HashtagRepository;
use Doctrine\ORM\NonUniqueResultException;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HashtagController extends AbstractController
{
    private $hashtagRepository;
    private $courcailletRepository;

    public function __construct(
        HashtagRepository $hashtagRepository,
        CourcailletRepository $courcailletRepository
    )
    {
        $this->hashtagRepository = $hashtagRepository;
        $this->courcailletRepository = $courcailletRepository;
    }

    /**
     * @Route(
     *     "/hashtag/{tag}",
     *     name="hashtag"
     * )
     * @param $tag
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return RedirectResponse|Response
     * @throws NonUniqueResultException
     */
    public function hashtag($tag, Request $request, PaginatorInterface $paginator)
    {
        //get hash by name (entity hash) en repo
        $query = $this->courcailletRepository->getCourcailletPerTag($tag);
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );
        $tagCount = $this->hashtagRepository->getCourcailletNumberPerTag($tag);
        return $this->render('default/hashtag.html.twig', [
            'tag'=> $tag,
            'tagCount'=>$tagCount,
            'pagination' => $pagination
        ]);
    }

    /**
     * Made for axios request (search bar request)
     * @Route("/axioshashtag/{search}")
     * @param $search
     * @return JsonResponse
     */
    public function userByPseudo($search)
    {
        //find the user with the matching pseudo in database
        $users = $this->hashtagRepository->getHashtagByNameInJson($search);
        //return users in a json array
        $response = new JsonResponse();
        $response->setData($users);
        return $response;
    }
}
