<?php

namespace App\Controller;

use App\Repository\CourcailletRepository;
use App\Repository\HashtagRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CourcailletController extends AbstractController
{
    private $entityManager;
    private $courcailletRepository;
    private $hashtagRepository;
    private $userRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        CourcailletRepository $courcailletRepository,
        HashtagRepository $hashtagRepository,
        UserRepository $userRepository
    )
    {
        $this->entityManager = $entityManager;
        $this->courcailletRepository = $courcailletRepository;
        $this->hashtagRepository = $hashtagRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @Route(
     *     "/delete/courcaillet/{courcailletId}",
     *     name="deleteCourcaillet"
     * )
     * @param $courcailletId
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function deleteCourcaillet ($courcailletId, Request $request)
    {
        $connectedUser = $this->getUser();
        $courcaillet = $this->courcailletRepository->find($courcailletId);
        //check if connected user is the owner of the courcaillet to delete
        $courcailletOwner = $courcaillet->getUserNbr();
        if($courcailletOwner->getId() == $connectedUser->getId()) {
            //suppress the courcaillet from the database
            $this->entityManager->remove($courcaillet);
            $this->entityManager->flush();
        }

        //redirect to previous route
        $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);
    }
}
