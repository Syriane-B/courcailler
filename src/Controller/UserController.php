<?php

namespace App\Controller;

use App\Entity\Courcaillet;
use App\Entity\Hashtag;
use App\Form\EditProfileType;
use App\Form\NewCourcailletType;
use App\Repository\CourcailletRepository;
use App\Repository\HashtagRepository;
use App\Repository\UserRepository;
use App\Security\UserAuthAuthenticator;
use App\Service\FollowerService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class UserController extends AbstractController
{
    private $params;
    private $entityManager;
    private $courcailletRepository;
    private $hashtagRepository;
    private $userRepository;
    private $followerService;

    public function __construct(
        EntityManagerInterface $entityManager,
        CourcailletRepository $courcailletRepository,
        HashtagRepository $hashtagRepository,
        UserRepository $userRepository,
        ParameterBagInterface $params,
        FollowerService $followerService
    )
    {
        $this->entityManager = $entityManager;
        $this->courcailletRepository = $courcailletRepository;
        $this->hashtagRepository = $hashtagRepository;
        $this->userRepository = $userRepository;
        $this->params = $params;
        $this->followerService = $followerService;
    }

    /**
     * @Route(
     *      "/profile/{id}",
     *      name="profile"
     * )
     * @param Request $request
     * @param int $id
     * @param PaginatorInterface $paginator
     * @return RedirectResponse|Response
     * @throws NonUniqueResultException
     */
    public function profile(Request $request, int $id, PaginatorInterface $paginator)
    {
        //get the current connected user
        $connectedUser = $this->getUser();

        //if the connected user match the user profile we want to see redirect to myProfile.html.twig
        if ($connectedUser->getId() === $id){
            //get the number of published courcaillet of the connected user
            $nbCourcaillet=$this->userRepository->getNbOfCourcaillet($id);
            //get followers and following
            $connectedUser->getFollowing();
            $connectedUser->getFollowers();
            //create a form to add courcaillet and deal with it when its completed
            $newCourcaillet = new Courcaillet();
            $form = $this->createForm(NewCourcailletType::class, $newCourcaillet);
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){

                $newCourcaillet=$form->getData();

                //set publication date
                $now = new DateTime();
                $newCourcaillet->setDate($now);
                //set the user
                $newCourcaillet->setUserNbr($connectedUser);

                //get hashtags
                $hashtags = $form->get('hashtags')->getData();
                $hashtags = strtolower ( $hashtags );
                $hashtags = str_replace(['#', ':', '.', '!', ',', '€'], ' ', $hashtags);
                //create an array containing each word
                $hashtags = explode(" ", $hashtags);
                //suppress empty key and re-index
                $hashtags=array_values(array_filter($hashtags));
                //Add courcaillet hashtag into the database and create the links
                if(!empty($hashtags)){
                    foreach ($hashtags as $hashtag){
                        //check in the database if its exist
                        $hashtagDataBase = $this->hashtagRepository->findOneBy(['name'=>$hashtag]);
                        //if it doesn't exist, create it
                        if($hashtagDataBase == null){
                            $hashtagDataBase = new Hashtag();
                            $hashtagDataBase->setName($hashtag);
                            $this->entityManager->persist($hashtagDataBase);
                        }
                        //and link it to the courcaillet
                        $newCourcaillet->addHashtag($hashtagDataBase);
                    }
                    $this->entityManager->flush();
                }
                //get mentions
                $mentions = $form->get('mention')->getData();
                $mentions = str_replace(['@'], ' ', $mentions);
                //create an array containing each word
                $mentions = explode(" ", $mentions);
                //suppress empty key and re-index
                $mentions = array_values(array_filter($mentions));
                if(!empty($mentions)){
                    foreach ($mentions as $pseudo){
                        //check in the database if its exist
                        $nameDataBase = $this->userRepository->findOneBy(['pseudo'=>$pseudo]);
                        //if the name exist, link the user with mention
                        if($nameDataBase !== null){
                            $newCourcaillet->addMention($nameDataBase);
                        }
                    }
                }
                $this->entityManager->persist($newCourcaillet);
                $this->entityManager->flush();
                return $this->redirect($request->getUri());
            }
            //get courcaillets of the user
            $query = $this->courcailletRepository->getCourcaillets($id);
            $pagination = $paginator->paginate(
                $query,
                $request->query->getInt('page', 1), /*page number*/
                10 /*limit per page*/
            );
            //if no new courcaillet added, show the connected user profile
            return $this->render('default/myProfile.html.twig', [
                'form'=>$form->createView(),
                'nbCourcaillets'=>$nbCourcaillet,
                'pagination' => $pagination
            ]);
        } else { //else, it's some one profile, show the profile
            //get the user profile information
            $user = $this->userRepository->find($id);
            //get the number of published courcaillet
            $nbCourcaillet = $this->userRepository->getNbOfCourcaillet($id);
            //get the courcaillets of the user
            $query = $this->courcailletRepository->getCourcaillets($id);
            $pagination = $paginator->paginate(
                $query,
                $request->query->getInt('page', 1), /*page number*/
                10 /*limit per page*/
            );
            //check if connected user follow the user
            $connectedUserIsFollowing = false;
            foreach ( $user->getFollowers() as $follower) {
                if ($follower->getId() == $connectedUser->getId()){
                    $connectedUserIsFollowing = true;
                }
            }
            return $this->render('default/profile.html.twig', [
                'user' => $user,
                'nbCourcaillets' => $nbCourcaillet,
                'pagination' => $pagination,
                'connectedUserIsFollowing' => $connectedUserIsFollowing
            ]);
        }
    }

    /**
     * @Route("/editprofile", name="editprofile")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function editprofile(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $connectedUser = $this->getUser();
        $form = $this->createForm(EditProfileType::class, $connectedUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if($form->get('plainPassword')->getData()){
                $connectedUser->setPassword(
                    $passwordEncoder->encodePassword(
                        $connectedUser,
                        $form->get('plainPassword')->getData()
                    )
                );
            }
            $this->entityManager->persist($connectedUser);
            $this->entityManager->flush();

            return $this->redirectToRoute('profile', ['id'=>$connectedUser->getId()]);
        }

        return $this->render('default/editProfile.html.twig', [
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route("/follow/{userId}",
     *     name="follow")
     * @param int $userId
     * @param Request $request
     * @return Response
     */
    public function follow($userId, Request $request)
    {
        $connectedUser = $this->getUser();
        $user = $this->userRepository->find($userId);
        if ($connectedUser->getId() !== $userId) {
            $connectedUser->addFollowing($user);

            $this->entityManager->persist($connectedUser);
            $this->entityManager->flush();
        }
        //redirect to previous route
        $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);
    }

    /**
     * @Route("/unfollow/{userId}",
     *     name="unfollow")
     * @param int $userId
     * @param Request $request
     * @return Response
     */
    public function unfollow($userId, Request $request)
    {
        $connectedUser = $this->getUser();
        $user = $this->userRepository->find($userId);
        if ($connectedUser->getId() !== $userId and $user) {
            $connectedUser->removeFollowing($user);

            $this->entityManager->persist($connectedUser);
            $this->entityManager->flush();
        }
        //redirect to previous route
        $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);
    }

    /**
     * @Route("/deleteprofile/{userId}", name="delete-profile")
     * @param int $userId
     * @return Response
     */
    public function deleteprofile(Request $request, int $userId)
    {
        //check if connected user match the user to delete
        $connectedUser = $this->getUser();
        if($connectedUser->getId() == $userId){
            $this->entityManager->remove($connectedUser);
            $this->entityManager->flush();
            //logout before redirecting
            $this->get('security.token_storage')->setToken(null);
            $request->getSession()->invalidate();

            //Create a flash message to inform the user the sucess of deleting its profile
            $this->addFlash('success', 'Votre profil et vos publications ont bien été supprimés.');
            return $this->redirectToRoute('app_login');
        }
        return $this->redirectToRoute('profile', ['id'=>$connectedUser->getId()]);
    }

    /**
     * Made for axios request (mentions in courcaillet creation)
     * @Route("/all-users")
     * @return JsonResponse
     */
    public function allDataBaseUsers()
    {
        $users = $this->userRepository->getUserInJson();
        //return users in a json array
        $response = new JsonResponse();
        $response->setData($users);
        return $response;
    }

    /**
     * Made for axios request (search bar request)
     * @Route("/users/{pseudo}")
     * @param $pseudo
     * @return JsonResponse
     */
    public function userByPseudo ($pseudo)
    {
        //find the user with the matching pseudo in database
        $users = $this->userRepository->getUserByPseudoInJson($pseudo);
        //return users in a json array
        $response = new JsonResponse();
        $response->setData($users);
        return $response;
    }
}
