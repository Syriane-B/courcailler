<?php

namespace App\Repository;

use Doctrine\ORM\Query;
use App\Entity\Hashtag;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;

/**
 * @method Hashtag|null find($id, $lockMode = null, $lockVersion = null)
 * @method Hashtag|null findOneBy(array $criteria, array $orderBy = null)
 * @method Hashtag[]    findAll()
 * @method Hashtag[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HashtagRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Hashtag::class);
    }

    /**
     * @param $tag
     * @return number
     * @throws NonUniqueResultException
     */
    public function getCourcailletNumberPerTag($tag){
        $qb = $this->createQueryBuilder('h')
            ->select('COUNT(c.id)')
            ->innerJoin('h.tag', 'c')
            ->andWhere('h.name=:tag')
            ->setParameter('tag', $tag)
            ->getQuery();
        return $qb->getSingleScalarResult();
    }

    /**
     * @param $search
     * @return mixed
     */
    public function getHashtagByNameInJson($search){
        $qb = $this->createQueryBuilder('h')
            ->where('h.name LIKE :search')
            ->setParameter('search', "%$search%")
            //limit results to 20
            ->setMaxResults(20)
            ->getQuery();
        return $qb->getResult(Query::HYDRATE_ARRAY);
    }

}
