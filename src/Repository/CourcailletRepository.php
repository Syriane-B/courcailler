<?php

namespace App\Repository;

use App\Entity\Courcaillet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * @method Courcaillet|null find($id, $lockMode = null, $lockVersion = null)
 * @method Courcaillet|null findOneBy(array $criteria, array $orderBy = null)
 * @method Courcaillet[]    findAll()
 * @method Courcaillet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CourcailletRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Courcaillet::class);
    }

    /**
     * @param $tag
     * @return Query
     */
    public function getCourcailletPerTag($tag)
    {
        $qb = $this->createQueryBuilder('c')
            ->select('c', 'u', 'h')
            ->leftJoin('c.hashtags', 'h')
            ->join('c.userNbr', 'u')
            ->andWhere('h.name=:tag')
            ->setParameter('tag', $tag)

            ->orderBy('c.date', 'DESC');

        return $qb->getQuery();
    }

    /**
     * @param null $userId
     * @return Query
     */
    public function getCourcaillets($userId = null)
    {
        $qb = $this->createQueryBuilder('c')
            ->select('c', 'u', 'h')
            ->join('c.userNbr', 'u')
            ->leftJoin('c.hashtags', 'h');

        if ($userId !== null ){
            $qb->where('u.id = :userId')
            ->setParameter('userId', $userId);
        }
        $qb = $qb ->orderBy('c.date', 'DESC');
        return $qb->getQuery();
    }

    /**
     * @param $followingUsersIdString
     * @return Query
     */
    public function getMostRecentPostFromFollowing($followingUsersIdString)
    {
        $qb = $this->createQueryBuilder('c')
            ->select('c')
            ->join('c.userNbr', 'u')
            ->orderBy('c.date', 'DESC')
            ->where("u.id IN ($followingUsersIdString)");
        return $qb->getQuery();
    }

    /**
     * @return Query
     */
    public function getLastCourcaillets()
    {
        $qb = $this->createQueryBuilder('c')
            ->select('c', 'u')
            ->join('c.userNbr', 'u')
            ->orderBy('c.date', 'DESC')
            ->setMaxResults(50);
        return $qb->getQuery();
    }
}
