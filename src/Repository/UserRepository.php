<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @param $id
     * @return int
     * @throws NonUniqueResultException
     */
    public function getNbOfCourcaillet($id)
    {
        $qb = $this->createQueryBuilder('u')
            ->select('COUNT(c.id)')
            ->innerJoin('u.courcaillets', 'c')
            ->where('u.id=:id')
            ->setParameter('id', $id)
            ->getQuery();
        return $qb->getSingleScalarResult();
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function getCourcaillets($userId){
        $qb = $this->createQueryBuilder('u')
            ->select('u','c', 'h')
            ->innerJoin('u.courcaillets', 'c')
            ->leftJoin('c.hashtags', 'h')
            ->where('u.id=userId')
            ->setParameter('userId', $userId)
            ->orderBy('c.date', 'DESC')
            ->getQuery();
        return $qb->getResult();
    }

    /**
     * @param $pseudo
     * @return mixed
     */
    public function findUserByName($pseudo){
        $qb = $this->createQueryBuilder('u')
                ->where('u.pseudo LIKE :pseudo')
                ->setParameter('pseudo', "%$pseudo%")
                ->getQuery();
        return $qb->getResult(Query::HYDRATE_ARRAY);
    }

    /**
     * @return mixed
     */
    public function getUserInJson(){
        $qb = $this->createQueryBuilder('u')
            ->getQuery();
        return $qb->getResult(Query::HYDRATE_ARRAY);
    }

    /**
     * @param $pseudo
     * @return mixed
     */
    public function getUserByPseudoInJson($pseudo){
        $qb = $this->createQueryBuilder('u')
            ->where('u.pseudo LIKE :pseudo')
            ->setParameter('pseudo', "%$pseudo%")
            //limit results to 20
            ->setMaxResults(20)
            ->getQuery();
        return $qb->getResult(Query::HYDRATE_ARRAY);
    }
}
