<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CourcailletRepository")
 */
class Courcaillet
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150)
     * @Assert\Length(
     *      min = 10,
     *      max = 140,
     *      minMessage = "Votre courcaillet doit contenir au minimum {{ limit }} caractères",
     *      maxMessage = "Votre courcaillet doit contenir au maximum {{ limit }} caractères"
     * )
     */
    private $content;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="courcaillets")
     * @ORM\JoinColumn(nullable=true)
     */
    private $userNbr;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User")
     */
    private $mention;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Hashtag", mappedBy="tag", cascade={"persist"})
     */
    private $hashtags;

    public function __construct()
    {
        $this->mention = new ArrayCollection();
        $this->hashtags = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getUserNbr(): ?User
    {
        return $this->userNbr;
    }

    public function setUserNbr(?User $userNbr): self
    {
        $this->userNbr = $userNbr;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getMention(): Collection
    {
        return $this->mention;
    }

    public function addMention(User $mention): self
    {
        if (!$this->mention->contains($mention)) {
            $this->mention[] = $mention;
        }

        return $this;
    }

    public function removeMention(User $mention): self
    {
        if ($this->mention->contains($mention)) {
            $this->mention->removeElement($mention);
        }

        return $this;
    }

    /**
     * @return Collection|Hashtag[]
     */
    public function getHashtags(): Collection
    {
        return $this->hashtags;
    }

    public function addHashtag(Hashtag $hashtag): self
    {
        if (!$this->hashtags->contains($hashtag)) {
            $this->hashtags[] = $hashtag;
            $hashtag->addTag($this);
        }

        return $this;
    }

    public function removeHashtag(Hashtag $hashtag): self
    {
        if ($this->hashtags->contains($hashtag)) {
            $this->hashtags->removeElement($hashtag);
            $hashtag->removeTag($this);
        }

        return $this;
    }
}
