<?php

namespace App\Entity;


use DateTime;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 * @Vich\Uploadable()
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="array")
     */
    private $roles = [];

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;

    /**
     * @ORM\Column(type="date")
     */
    private $birthday;

    /**
     * @ORM\Column(type="string", length=50, unique=true)
     */
    private $pseudo;

    /**
     * @ORM\Column(type="string", length=100, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $password;

    /**
     * @ORM\Column(type="date")
     */
    private $registerDate;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Courcaillet", mappedBy="userNbr", orphanRemoval=true)
     */
    private $courcaillets;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $avatarName;

    /**
     * @Vich\UploadableField(mapping="avatar", fileNameProperty="avatarName")
     * @var File
     */
    private $avatarFile;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var DateTime
     */
    private $avatarUpdatedAt;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="followers")
     */
    private $following;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="following")
     */
    private $followers;

    /**
     * @ORM\Column(type="boolean", options={"default" : false})
     */
    private $activeAccount;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\UserToken", mappedBy="User", cascade={"persist", "remove"})
     */
    private $userToken;

    /**
     * @return DateTime
     */
    public function getAvatarUpdatedAt(): DateTime
    {
        return $this->avatarUpdatedAt;
    }

    /**
     * @param DateTime $avatarUpdatedAt
     */
    public function setAvatarUpdatedAt(DateTime $avatarUpdatedAt): void
    {
        $this->avatarUpdatedAt = $avatarUpdatedAt;
    }

    public function __construct()
    {
        $this->courcaillets = new ArrayCollection();
        $this->following = new ArrayCollection();
        $this->followers = new ArrayCollection();
    }


    /**
     * @return string
     */
    public function getAvatarName(): ?string
    {
        return $this->avatarName;
    }

    /**
     * @param string $avatarName
     */
    public function setAvatarName(?string $avatarName): void
    {
        $this->avatarName = $avatarName;
    }

    /**
     * Returns the roles granted to the user.
     *
     * @return array (Role|string)[] The user roles
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt(){

    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername(){
        return $this->getPseudo();
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials(){

    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getRegisterDate(): ?\DateTimeInterface
    {
        return $this->registerDate;
    }

    public function setRegisterDate(\DateTimeInterface $registerDate): self
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * @return Collection|Courcaillet[]
     */
    public function getCourcaillets(): Collection
    {
        return $this->courcaillets;
    }

    public function addCourcaillet(Courcaillet $courcaillet): self
    {
        if (!$this->courcaillets->contains($courcaillet)) {
            $this->courcaillets[] = $courcaillet;
            $courcaillet->setUserNbr($this);
        }

        return $this;
    }

    public function removeCourcaillet(Courcaillet $courcaillet): self
    {
        if ($this->courcaillets->contains($courcaillet)) {
            $this->courcaillets->removeElement($courcaillet);
            // set the owning side to null (unless already changed)
            if ($courcaillet->getUserNbr() === $this) {
                $courcaillet->setUserNbr(null);
            }
        }
        return $this;
    }

    /**
     * @return File
     */
    public function getAvatarFile(): ?File
    {
        return $this->avatarFile;
    }

    /**
     * @param File $avatarFile
     * @throws \Exception
     */
    public function setAvatarFile(?File $avatarFile = null): void
    {
        $this->avatarFile = $avatarFile;
        $this->avatarUpdatedAt = new DateTime('now');
    }

    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->pseudo,
            $this->password,
            $this->email
        ));
    }
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->pseudo,
            $this->password,
            $this->email
            ) = unserialize($serialized);
    }

    /**
     * @return Collection|self[]
     */
    public function getFollowing(): Collection
    {
        return $this->following;
    }

    public function addFollowing(self $following): self
    {
        if (!$this->following->contains($following)) {
            $this->following[] = $following;
        }

        return $this;
    }

    public function removeFollowing(self $following): self
    {
        if ($this->following->contains($following)) {
            $this->following->removeElement($following);
        }

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getFollowers(): Collection
    {
        return $this->followers;
    }

    public function addFollower(self $follower): self
    {
        if (!$this->followers->contains($follower)) {
            $this->followers[] = $follower;
            $follower->addFollowing($this);
        }

        return $this;
    }

    public function removeFollower(self $follower): self
    {
        if ($this->followers->contains($follower)) {
            $this->followers->removeElement($follower);
            $follower->removeFollowing($this);
        }

        return $this;
    }

    public function getActiveAccount(): ?bool
    {
        return $this->activeAccount;
    }

    public function setActiveAccount(bool $activeAccount): self
    {
        $this->activeAccount = $activeAccount;

        return $this;
    }

    public function getUserToken(): ?UserToken
    {
        return $this->userToken;
    }

    public function setUserToken(UserToken $userToken): self
    {
        $this->userToken = $userToken;

        // set the owning side of the relation if necessary
        if ($this !== $userToken->getUser()) {
            $userToken->setUser($this);
        }

        return $this;
    }
}
