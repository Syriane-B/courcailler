<?php


namespace App\Service;

class GenerateTokenService
{
    public function generate(){
        //generate a token : shuffle the possible chars, take the 18 firsts
        $token = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!?-"), 0, 16);
        return $token;
    }
}