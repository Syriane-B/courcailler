<?php

namespace App\Service;
use Doctrine\ORM\Query;
use App\Entity\User;
use App\Repository\CourcailletRepository;

class FollowerService
{
    private $courcailletRepository;

    public function __construct(
        CourcailletRepository $courcailletRepository
    )
    {
        $this->courcailletRepository = $courcailletRepository;
    }

    /**
     * @param User $user
     * @return Query
     */
    public function getMostRecentPostFromFollowing(User $user)
    {
        $arrayFollowingUser = $user->getFollowing()->toArray();
        $followingUsersIdArray =[];
        foreach ($arrayFollowingUser as $followedUser){
            $followingUsersIdArray[] = $followedUser->getId();
        }
        $followingUsersIdString = implode(",", $followingUsersIdArray);
        return $this->courcailletRepository->getMostRecentPostFromFollowing($followingUsersIdString);
    }
}