<?php

namespace App\Service;

use App\Entity\User as AppUser;
use App\Exception\CustomAuthException;
use Symfony\Component\Security\Core\Exception;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserIsActive implements UserCheckerInterface
{
    public function checkPreAuth(UserInterface $user)
    {
        if (!$user->getActiveAccount()) {
            throw new CustomAuthException();
        }

        // user is deleted, show a generic Account Not Found message.
//        if ($user->isDeleted()) {
//            throw new AccountDeletedException();
//        }
    }

    public function checkPostAuth(UserInterface $user)
    {
        if (!$user instanceof AppUser) {
            return;
        }

        // user account is expired, the user may be notified
//        if ($user->isExpired()) {
//            throw new AccountExpiredException('...');
//        }
    }
}